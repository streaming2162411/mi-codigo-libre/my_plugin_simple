<?php
/* 
Plugin Name: Mi Plugin Simple
Plugin URI: https://www.solsitecinnova.com/tienda
Description: Mi primer plugin 
Version: 1.3.3
Author: Luis Torres Carpio
Author URI: https://www.solsitecinnova.com
License: GPLv2 
*/
namespace my_plugin_simple;

global $v1plug_prefix;
if(!isset($v1plug_prefix)) $v1plug_prefix = [];
$plug_dirname = dirname(plugin_basename(__FILE__));
// Prefix diferent Plugin Here !important
$v1plug_prefix[$plug_dirname] = "ltc1_";
// ****
add_action('admin_menu', function (){
	global $v1plug_prefix;
	$plug_dirname = dirname(plugin_basename(__FILE__));
	$plug_prefix = $v1plug_prefix[$plug_dirname];
	add_menu_page(
		'Mi Simple Plugin', //page title
        'Mi Simple Plugin', //menu title
        'manage_options', //capabilities
        $plug_prefix.'index', //menu slug
        'ltc_rdr_index', //function
        null,
    	null
    );
	add_menu_page(
		'Demo', //page title
        'Demo', //menu title
        'manage_options', //capabilities
        $plug_prefix.'demo', //menu slug
        'ltc_rdr_demo', //function
        null,
    	null
    );
});
if (!function_exists('ltclike')) {
	function ltclike($pattern, $string) {
		$pattern = preg_quote($pattern, '/');
		$pattern = str_replace('\%', '.*', $pattern); // Remover el \% para que no sea interpretado como un caracter especial
		$pattern .= '.*'; // Añadir .* al final para simular el comportamiento de LIKE 'Texto%'
		return (bool) preg_match('/^' . $pattern . '/i', $string);
	}
}
// Optimize - Only this Plugin execute
if(!ltclike($v1plug_prefix[$plug_dirname], @$_GET['page']) 
	&& !ltclike("do_".$v1plug_prefix[$plug_dirname], @$_GET['action']) 
	&& !ltclike($plug_dirname, @$_GET['plugin']) 
	) return;

register_activation_hook(__FILE__, function(){
	global $v1plug_prefix;
	global $wpdb;
	$plug_dirname = dirname(plugin_basename(__FILE__));
	$plug_prefix = $v1plug_prefix[$plug_dirname];

	$prefix = $wpdb->prefix . $plug_prefix;
	$sql = "CREATE TABLE IF NOT EXISTS {$prefix}prueba (
		  id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
		  nombres VARCHAR(50),
		  apellidos VARCHAR(50),
		  celular VARCHAR(15),
		  correo VARCHAR(100)
		)";
	$wpdb->query($sql);
});

register_deactivation_hook(__FILE__, function(){
	
});

add_action( "wp_ajax_do_{$v1plug_prefix[$plug_dirname]}controllerAux", $plug_dirname.'\ltc_directory' );
function ltc_directory(){
	global $v1plug_prefix;
	$plug_dirname = dirname(plugin_basename(__FILE__));
	$plug_prefix = $v1plug_prefix[$plug_dirname];
	
	
	if(isset($_REQUEST['controller'])){
		include('includes/'.$_REQUEST['controller'].'.php');
		eval("ltc_rdr_".$_REQUEST['controller']."();");
	}else{
		include('init.php');
		$plugin_path = plugin_dir_path( __FILE__ );
		foreach (glob("{$plugin_path}/includes/*.php") as $file) {
			include $file;
		}
	}
}
add_action('admin_menu', $plug_dirname.'\ltc_directory');