<?php
function ltc_rdr_demo() { 
	$q = $_GET; $b = $_POST;
	
	if(isset($_REQUEST['accion'])){
		switch($_REQUEST['accion']){
			case 'saludar':
				echo "Procesando... Restart en 2 segundos.";
				echo '<meta http-equiv="refresh" content="2; url='.get_site_url().'/wp-admin/admin.php?page='.$q["page"].'" />';
				break;

			case 'saludar_ajax':
				$nombre = $b['nombre'];
				echo json_encode([
					"success"=>1,
					"message"=>"Saludo enviado por: {$nombre}"
				]); die();
				break;
		}
		exit;
	}
	
?>
<div class="section">
	<h1>Inicio de la Interface</h1>
	<form action="" method="POST">
		<input type="hidden" name="accion" value="saludar">
		<input type="text" name="nombre">
		<button type="submit">Enviar</button>
		<button type="button" onclick="saludar_ajax()">Enviar Ajax</button>
	</form>

	<script>
		$(document).ready(function() {
			console.log("JQUERY ready!")
		});
		function saludar_ajax(){
			var nombre = $("input[name=nombre]").val()
			$.post(he.con('demo@saludar_ajax'), {
				nombre: nombre
			}, function(res) {
				console.log(res)
				if(res.success){
					alert(res.message);
				}
			},"json");
		}
	</script>
</div>


<?php } ?>